-- wezterm cli send-text
local wezterm = require("wezterm")

local mykeys = {
	{ key = "p", mods = "SHIFT|ALT", action = "ActivateCopyMode" },
	{ key = "c", mods = "SHIFT|ALT", action = wezterm.action({ CloseCurrentPane = { confirm = true } }) },
	{ key = "q", mods = "SHIFT|CTRL", action = wezterm.action({ CloseCurrentTab = { confirm = true } }) },
	{
		key = '"',
		mods = "SHIFT|ALT",
		action = wezterm.action({ SplitVertical = { domain = "CurrentPaneDomain" } }),
	},
	{
		key = "%",
		mods = "SHIFT|ALT",
		action = wezterm.action({ SplitHorizontal = { domain = "CurrentPaneDomain" } }),
	},
	{ key = "h", mods = "ALT", action = wezterm.action({ ActivatePaneDirection = "Left" }) },
	{ key = "l", mods = "ALT", action = wezterm.action({ ActivatePaneDirection = "Right" }) },
	{ key = "k", mods = "ALT", action = wezterm.action({ ActivatePaneDirection = "Up" }) },
	{ key = "j", mods = "ALT", action = wezterm.action({ ActivatePaneDirection = "Down" }) },
}
for i = 1, 8 do
	-- CTRL+ALT + number to activate that tab
	table.insert(mykeys, {
		key = tostring(i),
		mods = "ALT",
		action = wezterm.action({ ActivateTab = i - 1 }),
	})
end

return {
	inactive_pane_hsb = {
		saturation = 0.5,
		brightness = 0.5,
	},
	font_size = 9,
	font = wezterm.font("JetBrainsMono Nerd Font Mono"),
	keys = mykeys,
}
