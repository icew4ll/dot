[ -z ${DISPLAY+x} ] || xset r rate 200 140
cfg=$HOME/.dot
py_ver=$(pyenv which python | perl -pe "s;^.*versions/(.*?)/.*;\1;")
py_path=$HOME/.pyenv/versions/$py_ver/bin
export NNN_PLUG="r:skrg;f:skopen;j:autojump;p:preview-tui"
export EDITOR=nvim
export GOPATH=~/go
export GOBIN=$(go env $GOPATH)/bin
export MANPAGER='nvim +Man!'
export NEXT_TELEMETRY_DEBUG=1
export BOOKMARKS_DIR="~/.config/nnn/bookmarks"
export TERM="xterm-256color"
export RUST_SRC_PATH=$(rustc --print sysroot)/lib/rustlib/src/rust/library
export GITPAGER="bat"
export RG_PREFIX="rg --column --line-number --no-heading --color=always --smart-case"
export FZF_DEFAULT_COMMAND="fd --type f"
export PYENV_ROOT=~/.pyenv
export PATH=$PATH:$GOPATH/bin:$py_path:$HOME/.cargo/bin

alias l="lsd"
alias fl="fc-list"
alias la="lsd -la"
alias n="nnn"
alias tz="source ~/.bashrc"
alias ta="$EDITOR ~/.bashrc"
alias tn="$EDITOR ~/.config/nimdow/config.toml"
alias ac="$EDITOR ~/.config/alacritty/alacritty.yml"
alias dot="/usr/bin/git --git-dir=$cfg --work-tree=$HOME"
eval "$(zoxide init bash)"
eval "$(starship init bash)"
