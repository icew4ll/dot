#!/bin/bash
e1() {
  xprop -spy -root -notype _NET_ACTIVE_WINDOW \
  | stdbuf -oL cut -d ' ' -f5 \
  | xargs -n1 xprop -id \
  | stdbuf -oL grep -F _NET_WM_NAME \
  | stdbuf -oL sed 's|^.* = \"\(.*\)\"$|\1|' \
  | xargs -n1 echo
  # | xargs -I% sed -i "1 s|.*|%|" /tmp/window
}
e2() {
  dkcmd status type=win
}
e2
