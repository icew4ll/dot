#!/bin/bash
e2() {
  dkcmd status type=ws
  dkcmd status type=win
  dkcmd status type=layout
  dkcmd status type=bar
}

e3 () {
tee <<'EOF'
(box
:orientation "h"
:space-evenly false
:spacing 5
(label :text "⚪") 
(label :text "")
(label :text "")
(label :text "")
(label :text "")
(label :text "")
)
EOF
}

e5 () {
dkcmd status type=ws | while read -r line; do
  IFS=":" read -ra ARR <<< "$line"
  for i in "${ARR[@]}"; do
    [[ $i = A* ]] && printf "🟨"
    [[ $i = a* ]] && printf "🟫"
    [[ $i = I* ]] && printf "🟡"
    [[ $i = i* ]] && printf "🟤"
  done
done
}

e6 () {
  # parse line output
  dkcmd status type=ws | while read -r line; do
    # iterate on ":"
    IFS=":" read -ra ARR <<< "$line"
      { tee <<'EOF'
      (box
      :orientation "h"
      :space-evenly false
      :spacing 5
EOF
      n=0
      for i in "${ARR[@]}"; do
        ((n++))
        [[ $i = A* ]] && icon="🟨"
        [[ $i = a* ]] && icon="🟫"
        [[ $i = I* ]] && icon="🟡"
        [[ $i = i* ]] && icon="🟤"
        click="dkcmd ws view $n"
        btn="(button :onclick \"$click\" \"$icon\")"
        echo "$btn"
      done
      echo ")"
      } | tr "\n" " " | sed "s|$|\n|"
  done
}

e7 () {
  n=0
  while true; do
    ((n++))
    echo "$n"
      sleep 1
  done
}

e0 () {
  dkcmd status type=ws | while read -r _; do
  { tee <<'EOF'
  (box
  :orientation "h"
  :space-evenly false
  :spacing 5
  (button :onclick "notify-send noice" "⚪")
  (button :onclick "notify-send noice" "")
  )
EOF
  } | tr "\n" " " | sed "s|$|\n|"
  done
}
e6

# echo 	"(box	:class \"works\"	:orientation \"h\" :spacing 5 :space-evenly \"false\" (button :onclick \"bspc desktop -f "$ws1"\"	:class	\""$un$o1$f1"\"	\""$ic_1"\") (button :onclick \"bspc desktop -f "$ws2"\"	:class \""$un$o2$f2"\"	 \""$ic_2"\") (button :onclick \"bspc desktop -f "$ws3"\"	:class \""$un$o3$f3"\" \""$ic_3"\") (button :onclick \"bspc desktop -f "$ws4"\"	:class \""$un$o4$f4"\"	\""$ic_4"\") (button :onclick \"bspc desktop -f "$ws5"\"	:class \""$un$o5$f5"\" \""$ic_5"\")  (button :onclick \"bspc desktop -f "$ws6"\"	:class \""$un$o6$f6"\" \""$ic_6"\"))"
