#!/bin/bash
green() {
        echo -e "\e[0;32m$1\e[0m"
}

yellow() {
        echo -e "\e[1;33m$1\e[0m"
}

red() {
        echo -e "\e[1;31m$1\e[0m"
}

f1() {
        while true; do
                echo "WA1:a2:a3:i4:i5:i6:i7:i8:i9:i10"
                echo "Ltile"
                echo "Anu"
                sleep 1
                echo "WA1:a2:a3:i4:i5:i6:i7:i8:i9:i10"
                echo "Ltile"
                echo "Aarch@archlinux:~"
                sleep 1
        done
}

f2() {
        f1 | while read -r LINE; do
                yellow "$LINE"
                [[ $LINE = W* ]] && t1="$LINE"
                [[ $LINE = L* ]] && t2="$LINE"
                [[ $LINE = A* ]] && t3="$LINE"
                echo "1"
                [ "${t1+x}" != "" ] && [ "${t2+x}" != "" ] && [ "${t3+x}" != "" ] && {
                        red "$t1 $t2 $t3"
                }
        done
}
f3() {
        n=0
        f1 | while read -r LINE; do
                ((n++))
                yellow "$n $LINE"

                [[ $LINE = W* ]] && t1="$LINE"
                [[ $LINE = L* ]] && t2="$LINE"
                [[ $LINE = A* ]] && t3="$LINE"
                [ "${t1+x}" != "" ] && [ "${t2+x}" != "" ] && [ "${t3+x}" != "" ] && {
                        red "$t1 $t2 $t3"
                }
        done
}
f4() {
        n=0
        ARR=()
        f1 | while read -r LINE; do
                ((n++))
                yellow "$n $LINE"
                ARR+=("$LINE")
                # extract full state
                if [[ $n == 3 ]]; then
                        WORK="${ARR[0]}"
                        LAY="${ARR[1]}"
                        WIN="${ARR[2]}"
                        echo "$WORK" "$LAY" "$WIN"
                        # reset
                        n=0 && ARR=()
                fi
                # for i in "${ARR[@]}"; do
                #       echo "$i"
                # done
        done
}
# LATEST
workspace() {
  IFS=":" read -ra ARR_WIN <<<"$1"
  n=0
  for i in "${ARR_WIN[@]}"; do
    ((n++))
    [[ $i = A* ]] && icon="🟩"
    [[ $i = a* ]] && icon="🟫"
    [[ $i = I* ]] && icon="🟢"
    [[ $i = i* ]] && icon="🟤"
    click="dkcmd ws view $n"
    btn="(button :onclick \"$click\" \"$icon\")"
    echo "$btn"
  done
}
layout() {
  case "$1" in
    tile) icon="📑"
    ;;
    grid) icon="🔳"
    ;;
    *) icon="🌼"
    ;;
  esac
echo "(label :text \"$icon\" :class \"layout\" )"
}
f5() {
  n=0
  ARR=()
  dkcmd status type=bar | while read -r LINE; do
    ((n++))
    # yellow "$n $LINE"
    ARR+=("$LINE")
    # extract full state
    if [[ $n == 3 ]]; then
      WORK="${ARR[0]:1}"
      LAY="${ARR[1]:1}"
      WIN="${ARR[2]:1}"
      # echo "$WORK" "$LAY" "$WIN"
      # BUILD
      {
        tee <<'EOF'
        (box
        :space-evenly false
        :spacing 0
EOF
      layout "$LAY"
      workspace "$WORK"
      echo "(label :text \"$WIN\" :class \"window\" )"
      echo ")"
      } | tr "\n" " " | sed "s|$|\n|"
      # } | tr "\n" " " | sed "s|$|\n|"

      # reset
      n=0 && ARR=()
    fi
          # for i in "${ARR[@]}"; do
          #       echo "$i"
          # done
  done
}
f5
# dkcmd status type=win | while read -rn 3 LINE; do echo "$LINE" | tr "\n" " ";done
# dkcmd status type=win | while read -rn 3 LINE; do [ "$LINE" != "" ] && echo "$LINE";done
# dkcmd status type=win | while read -rn 3 LINE; do [ "$LINE" != "" ] && echo "$LINE";done
# dkcmd status type=win | while read -rn 3 LINE; do echo "$LINE";done
# dkcmd status type=ws | while read -r LINE; do eww open bar;done
