#!/bin/bash

e1() {
  # parse line output
  dkcmd status type=bar | grep -E "^W.*" | while read -r line; do
    # iterate on ":"
    IFS=":" read -ra ARR <<< "$line"
      { tee <<'EOF'
      (box
      :orientation "h"
      :space-evenly false
      :spacing 5
EOF
      n=0
      for i in "${ARR[@]}"; do
        ((n++))
        [[ $i = A* ]] && icon="🟨"
        [[ $i = a* ]] && icon="🟫"
        [[ $i = I* ]] && icon="🟡"
        [[ $i = i* ]] && icon="🟤"
        click="dkcmd ws view $n"
        btn="(button :onclick \"$click\" \"$icon\")"
        echo "$btn"
      done
      echo ")"
      } | tr "\n" " " | sed "s|$|\n|"
  done
}

e2() {
  # parse unit of output
  dkcmd status type=bar | while read -ra unit; do
  echo "${unit[0]}"
  # readarray -t y <<<"$unit"
  # echo "$y"

    # echo "${ARR[0]}" | sed -n 1p
  # work=$(echo "$unit" | grep -E "^W")
  # echo "$work" | tr -d "\n"
    # IFS=$'\n' read -ra ARR <<< "$unit"
    #   echo "${ARR[0]}"
      # n=0
      # for i in "${ARR[@]}"; do
      #   ((n++))
      #   echo "$i" "$n"
      # done
  # echo "$line" | tr "\n" "X"
  # work=$(echo "$line" | grep -E "^W")
  # lay=$(echo "$line" | grep -E "^L")
  # echo "$work" | tr "\n" "X"
  # echo "$lay" | tr "\n" "X"
  done
}
e3() {
echo -n "Enter the desired vowel: "
read -r ch       # will be lowercase no matter what they enter
case "$ch" in # so we only have to match lower cases
[aeiou]) echo "vowel" ;; # reads stdin, writes to stdout
*) echo "That's not a valid vowel" >&2 # write to STDERR to keep separateA
;; esac
}
e4() {
  dkcmd status type=bar | while read -r UNIT; do
    # echo "$UNIT"  
    [[ $UNIT = W* ]] && WORK="$UNIT"
    [[ $UNIT = L* ]] && LAY="$UNIT"
    [[ $UNIT = A* ]] && WIN="$UNIT"
    echo "$WORK" "$LAY" "$WIN"
  done
}
e5() {
  dkcmd status type=bar | while read -r UNIT; do
    # echo "$UNIT"  
    # [[ $UNIT = W* ]] && WORK=$(UNIT | sed "s|^W||")
    [[ $UNIT = W* ]] && WORK="$UNIT"
    [[ $UNIT = L* ]] && LAY="$UNIT"
    [[ $UNIT = A* ]] && WIN="$UNIT"
    [[ -z ${WORK+x} && -z ${LAY+x} && -z ${WIN+x} ]] && echo "$WORK" "$LAY" "$WIN"
  done
}
e6() {
  dkcmd status type=bar | while read -r UNIT; do
    # echo "$UNIT"  
    # [[ $UNIT = W* ]] && WORK=$(UNIT | sed "s|^W||")
    [[ $UNIT = W* ]] && WORK="$UNIT"
    [[ $UNIT = L* ]] && LAY="$UNIT"
    [[ $UNIT = A* ]] && WIN="$UNIT"
    [[ -z ${WORK+x} && -z ${LAY+x} && -z ${WIN+x} ]] && echo "$WORK" "$LAY" "$WIN"
  done
}
e6
