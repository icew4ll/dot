export def-env zi [] {
  cd (zoxide query --exclude ($env.PWD) -- | str collect | str trim)
}
export def-env z  [] {
  cd (zoxide query -i -- | str collect | str trim)
}
export def-env za  [] {
  zoxide add -- (shells | where active == true | get path | get 0)
}
# export def-env nf  [] {
#   # sh ~/.config/nnn/plugins/open 
#   cd ~/m/vim ; nnn
#   # cd ~/m/vim ; bash ~/.config/nnn/plugins/open
# }
export def-env mks [file: string] {
  echo "#!/bin/bash" | save $"($file).sh"
  ^$"($env.EDITOR) ($file).sh"
}
# export def-env pi [pkg: string] {
#   $"($env.PY_BIN) -m pip install ($pkg)"
# }

export def-env upa [] {
  [
  ~/.config/waybar,
  ~/.config/hikari,
  ~/.config/dunst,
  ~/.config/river,
  ~/.config/mako,
  ~/.config/foot,
  ~/.config/fastfetch,
  ~/.config/dk,
  ~/.config/nvim,
  ~/.config/eww,
  ~/.config/kitty,
  ~/.config/neofetch,
  ~/.config/bspwm,
  ~/.config/dk,
  ~/.config/sxhkd,
  ~/.config/nushell,
  ~/.config/nnn,
  ~/.config/rofi,
  ~/.config/qt5ct,
  ~/.config/nimdow,
  ~/.config/picom,
  ~/.config/alacritty,
  ~/.config/gtk-2.0,
  ~/.config/gtk-3.0,
  ~/.config/gtk-4.0,
  ~/m/hax
  ~/.bashrc,
  ~/.xprofile,
  ~/.gitconfig,
  ~/.gitignore,
  ~/.bashrc,
  ~/.xprofile,
  ~/.Xresources,
  ~/.gtkrc-2.0,
  ~/.wezterm.lua,
  ] | each { |x| dot add $x}
  dot status
}
export def-env upc [commit: string] {
  dot commit -m $commit
  dot push
}
