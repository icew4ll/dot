# settings
{
PY_VER: (pyenv which python | path dirname | path dirname | path basename)
PY_BIN: (pyenv which python)
EDITOR: "nvim"
VISUAL: "nvim"
DOT: $"($env.HOME)/.dot"
MANPAGER: 'nvim +Man!'
NNN_PLUG: "r:rg;f:open;j:autojump;p:preview-tui"
NNN_FIFO: "/tmp/nnn.fifo"
} | load-env

{
PATH: [
/usr/local/sbin
/usr/local/bin
/usr/bin
$"($env.HOME)/go/bin"
$"($env.HOME)/.gnupg"
$"($env.HOME)/.volta/bin"
$"($env.HOME)/.cargo/bin"
$"($env.HOME)/.pyenv/versions/($env.PY_VER)/bin"
]
} | load-env

# starship
{
STARSHIP_SHELL: "nu"
PROMPT_COMMAND: { starship prompt --cmd-duration $env.CMD_DURATION_MS --status $env.LAST_EXIT_CODE | str trim
}
# PROMPT_COMMAND: { starship }
PROMPT_INDICATOR: " "
PROMPT_INDICATOR_VI_INSERT: "🌲"
PROMPT_INDICATOR_VI_NORMAL: "🦝"
PROMPT_MULTILINE_INDICATOR: "::: "
} | load-env

# oh-my-posh
# let-env PROMPT_COMMAND = { echo $(oh-my-posh --config ~/.poshthemes/M365Princess.omp.json) }
# let-env PROMPT_COMMAND = { oh-my-posh --config ~/.config/blueish.omp.json | str collect }
