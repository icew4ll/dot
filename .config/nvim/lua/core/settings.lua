local flat = require("core.flat")
local g = vim.g

-- Create cache dir and subs dir
local createdir = function()
	local data_dir = {
		flat.cache_dir .. "backup",
		flat.cache_dir .. "session",
		flat.cache_dir .. "swap",
		flat.cache_dir .. "tags",
		flat.cache_dir .. "undo",
	}
	-- There only check once that If cache_dir exists
	-- Then I don't want to check subs dir exists
	if vim.fn.isdirectory(flat.cache_dir) == 0 then
		os.execute("mkdir -p " .. flat.cache_dir)
		for _, v in pairs(data_dir) do
			if vim.fn.isdirectory(v) == 0 then
				os.execute("mkdir -p " .. v)
			end
		end
	end
end

local disable_distribution_plugins = function()
	g.loaded_gzip = 1
	g.loaded_tar = 1
	g.loaded_tarPlugin = 1
	g.loaded_zip = 1
	g.loaded_zipPlugin = 1
	g.loaded_getscript = 1
	g.loaded_getscriptPlugin = 1
	g.loaded_vimball = 1
	g.loaded_vimballPlugin = 1
	g.loaded_matchit = 1
	g.loaded_matchparen = 1
	g.loaded_2html_plugin = 1
	g.loaded_logiPat = 1
	g.loaded_rrhelper = 1
	g.loaded_netrw = 1
	g.loaded_netrwPlugin = 1
	g.loaded_netrwSettings = 1
	g.loaded_netrwFileHandlers = 1
end

local leader_map = function()
	vim.g.mapleader = " "
	vim.api.nvim_set_keymap("n", " ", "", { noremap = true })
	vim.api.nvim_set_keymap("x", " ", "", { noremap = true })
end


local load_core = function()
	createdir()
	disable_distribution_plugins()
	leader_map()
end

load_core()
