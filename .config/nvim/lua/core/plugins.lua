-- Autocommand that reloads neovim whenever you save the plugins.lua file
vim.cmd([[
  augroup packer_user_config
    autocmd!
    autocmd BufWritePost core.plugins.lua source <afile> | PackerSync
  augroup end
]])

-- Use a protected call so we don't error out on first use
local ok, packer = pcall(require, "packer")
if not ok then
	return
end

-- Have packer use a popup window
packer.init({
	compile_path = vim.fn.stdpath("config") .. "/lua/packer_compiled.lua",
	display = {
		open_fn = function()
			return require("packer.util").float({ border = "rounded" })
		end,
	},
})

require("packer").startup({
	function(use)
		-- Packer can manage itself
		use {"wbthomason/packer.nvim", opt = true}
		use("lewis6991/impatient.nvim")
		use("delphinus/agrp.nvim")
		use("delphinus/mappy.nvim")
	end,
})
