return {
	{
		"hrsh7th/nvim-cmp",
		-- after = "lspkind-nvim",
		after = "friendly-snippets",
		config = function()
			local present, cmp = pcall(require, "cmp")

			if not present then
				return
			end

			-- vim.opt.completeopt = "menuone,noselect"

			-- nvim-cmp setup
			cmp.setup({
				snippet = {
					expand = function(args)
						require("luasnip").lsp_expand(args.body)
					end,
				},
				formatting = {
					format = function(entry, vim_item)
						-- load lspkind icons
						vim_item.kind = string.format(
							"%s %s",
							require("modules.lspkind").icons[vim_item.kind],
							vim_item.kind
						)

						vim_item.menu = ({
							buffer = "🌲",
							nvim_lsp = "🔮",
							nvim_lua = "🌙",
							path = "🧭",
							luasnip = "📜",
							tmux = "💻",
							emoji = "🍵",
						})[entry.source.name]

						return vim_item
					end,
				},
				mapping = {
					["<C-p>"] = cmp.mapping.select_prev_item(),
					["<C-n>"] = cmp.mapping.select_next_item(),
					["<C-d>"] = cmp.mapping.scroll_docs(-4),
					["<C-f>"] = cmp.mapping.scroll_docs(4),
					["<C-Space>"] = cmp.mapping.complete(),
					["<C-e>"] = cmp.mapping.close(),
					["<CR>"] = cmp.mapping.confirm({
						behavior = cmp.ConfirmBehavior.Replace,
						select = true,
					}),
					["<Tab>"] = function(fallback)
						if cmp.visible() then
							cmp.select_next_item()
						elseif require("luasnip").expand_or_jumpable() then
							vim.fn.feedkeys(
								vim.api.nvim_replace_termcodes("<Plug>luasnip-expand-or-jump", true, true, true),
								""
							)
						else
							fallback()
						end
					end,
					["<S-Tab>"] = function(fallback)
						if cmp.visible() then
							cmp.select_prev_item()
						elseif require("luasnip").jumpable(-1) then
							vim.fn.feedkeys(
								vim.api.nvim_replace_termcodes("<Plug>luasnip-jump-prev", true, true, true),
								""
							)
						else
							fallback()
						end
					end,
				},
				sources = {
					{ name = "nvim_lsp" },
					{ name = "nvim_lsp_signature_help" },
					{ name = "luasnip" },
					{ name = "buffer" },
					{ name = "nvim_lua" },
					{ name = "path" },
					{ name = "tmux" },
					{ name = "emoji" },
					{ name = "orgmode" },
				},
			})
		end,
	},
	{
		"saadparwaiz1/cmp_luasnip",
		after = "LuaSnip",
	},
	{
		"hrsh7th/cmp-nvim-lua",
		after = "cmp_luasnip",
	},
	{
		"hrsh7th/cmp-nvim-lsp",
		after = "cmp-nvim-lua",
	},
	{
		"hrsh7th/cmp-buffer",
		after = "cmp-nvim-lsp",
	},
	{
		"hrsh7th/cmp-path",
		after = "cmp-buffer",
	},
	{
		"andersevenrud/compe-tmux",
		after = "cmp-path",
	},
	{
		"hrsh7th/cmp-emoji",
		after = "compe-tmux",
	},
	{
		"hrsh7th/cmp-cmdline",
		after = "nvim-cmp",
	},
	{
		"windwp/nvim-autopairs",
		config = function()
			require("nvim-autopairs").setup({
				disable_filetype = { "TelescopePrompt", "vim" },
			})
			local cmp_autopairs = require("nvim-autopairs.completion.cmp")
			local cmp = require("cmp")
			cmp.event:on("confirm_done", cmp_autopairs.on_confirm_done({ map_char = { tex = "" } }))
		end,
		event = "InsertEnter",
	},
	{
		"onsails/lspkind-nvim",
		event = "InsertEnter",
	},
	{
		"rafamadriz/friendly-snippets",
		event = "InsertEnter",
	},
	{
		"L3MON4D3/LuaSnip",
		wants = "friendly-snippets",
		after = "nvim-cmp",
		config = function()
			local present, luasnip = pcall(require, "luasnip")
			if not present then
				return
			end

			luasnip.config.set_config({
				history = true,
				updateevents = "TextChanged,TextChangedI",
			})

			require("luasnip/loaders/from_vscode").load()
			require("modules.snips")
			-- require("modules.snips")
		end,
	},
}
