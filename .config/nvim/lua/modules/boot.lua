return {
	-- TODO: needed here?
	{ "nvim-lua/plenary.nvim" },
	"delphinus/agrp.nvim",
	"delphinus/mappy.nvim",
	{ "lewis6991/impatient.nvim" },
	{ "wbthomason/packer.nvim", opt = true },
}
