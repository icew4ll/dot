local ls = require("luasnip")
local s = ls.s
local p = require("luasnip.extras").partial
local t = ls.text_node
local i = ls.insert_node
-- local f = ls.function_node

-- ls.add_snippets

ls.snippets = {
	all = {
		s({ trig = "ymd", name = "Current date", dscr = "Insert the current date" }, {
			p(os.date, "%Y-%m-%d"),
		}),
	},
	javascript = {
		s({ trig = "rfc", name = "react functional component", dscr = "react functional component" }, {
			t({ '"""', "" }),
			i(1),
			t({ "", '"""', "", "" }),
			i(0),
		}),
		s({ trig = "init", name = "Init", dscr = "Init" }, {
			t({ '"""', "" }),
			i(1),
			t({ "", '"""', "", "", "" }),
			t({ "def main():", "\t\t" }),
			t({ '"""main"""', "\t\t" }),
			i(2),
			t({ "", "", "", "" }),
			t({ 'if __name__ == "__main__":', "\t\t" }),
			i(0),
			t("main()"),
		}),
	},
	python = {
		s({ trig = "doc", name = "doc", dscr = "doc" }, {
			t({ '"""', "" }),
			i(1),
			t({ "", '"""', "", "" }),
			i(0),
		}),
		s({ trig = "init", name = "Init", dscr = "Init" }, {
			t({ '"""', "" }),
			i(1),
			t({ "", '"""', "", "", "" }),
			t({ "def main():", "\t\t" }),
			t({ '"""main"""', "\t\t" }),
			i(2),
			t({ "", "", "", "" }),
			t({ 'if __name__ == "__main__":', "\t\t" }),
			i(0),
			t("main()"),
		}),
	},
	sh = {
		s({ trig = "fn", name = "fn", dscr = "fn" }, {
			i(1),
			i(0),
			t({"() {", ""}),
			t("}"),
		}),
		s({ trig = "ch", name = "Checklist", dscr = "Checklist" }, {
			t("- [ ] "),
			i(1),
			i(0),
		}),
	},
	org = {
		s({ trig = "bash", name = "bash", dscr = "bash" }, {
			t({"#+BEGIN_SRC bash", ""}),
			i(1),
			i(0),
			t({"","#+END_SRC"}),
		}),
		s({ trig = "lk", name = "Link", dscr = "Link" }, {
			t("[["),
			i(1),
			t("]["),
			i(2),
			i(0),
			t("]]"),
		}),
		s({ trig = "ch", name = "Checklist", dscr = "Checklist" }, {
			t("- [ ] "),
			i(1),
			i(0),
		}),
	},
}
