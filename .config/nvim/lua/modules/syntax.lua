return {
	{
		"michaelb/sniprun",
		run = "bash ./install.sh",
	},
	{
		"LhKipp/nvim-nu",
		run = ":TSUpdate",
		config = function()
      require('nu').setup({
        complete_cmd_names = true,
      })
    end,
	},
	{
		"elkowar/yuck.vim",
	},
	-- {
	-- 	"Olical/conjure",
	-- 	opt = true,
	-- 	event = "BufEnter",
	-- 	-- event = { "FocusLost", "CursorHold" },
	-- },
	{
		"folke/trouble.nvim",
		opt = true,
		-- event = "BufReadPre",
		event = { "FocusLost", "CursorHold" },
		cmd = { "TroubleToggle", "Trouble" },
		requires = "kyazdani42/nvim-web-devicons",
	},
	{
		"vmchale/ion-vim",
		ft = "ion",
		opt = true,
		event = "BufEnter",
	},
	{
		"dmix/elvish.vim",
		ft = "ion",
		opt = true,
		event = "BufEnter",
	},
	{
		"ollykel/v-vim",
		ft = "vlang",
		opt = true,
		event = "BufEnter",
	},
	{
		"norcalli/nvim-colorizer.lua",
		opt = true,
		event = "BufRead",
		config = function()
			require("colorizer").setup({
				-- "*", -- enable on all files
				css = { rgb_fn = true },
				scss = { rgb_fn = true },
				sass = { rgb_fn = true },
				stylus = { rgb_fn = true },
				vim = { names = true },
				tmux = { names = false },
				"javascript",
				"javascriptreact",
				"typescript",
				"typescriptreact",
				"conf",
				lua = { rgb_fn = true },
			})
		end,
	},
	{
		"terrortylor/nvim-comment",
		opt = true,
		event = "BufRead",
		config = function()
			require("nvim_comment").setup({
				hook = function()
					if vim.api.nvim_buf_get_option(0, "filetype") == "vue" then
						require("ts_context_commentstring.internal").update_commentstring()
					end
					if vim.api.nvim_buf_get_option(0, "filetype") == "typescriptreact" then
						require("ts_context_commentstring.internal").update_commentstring()
					end
					if vim.api.nvim_buf_get_option(0, "filetype") == "javascriptreact" then
						require("ts_context_commentstring.internal").update_commentstring()
					end
				end,
			})
		end,
	},
	{
		"lukas-reineke/indent-blankline.nvim",
		opt = true,
		event = "BufEnter",
	},
}
