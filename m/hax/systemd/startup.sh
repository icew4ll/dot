#!/bin/bash

# create systemd service
file=/etc/systemd/system/startup.service
sudo rm $file
file=/etc/systemd/system/dunst.service
sudo tee "$file" <<- EOF
[Unit]
Description=Dunst notification daemon
Documentation=man:dunst(1)
PartOf=graphical-session.target
After=graphical-session.target

[Service]
Type=dbus
BusName=org.freedesktop.Notifications
ExecStart=/usr/bin/dunst
Environment=DISPLAY=:0
    
[Install]
WantedBy=graphical-session.target
EOF

sudo systemctl daemon-reload
sudo systemctl restart dunst.service
# sudo systemctl enable --now startup.service
# journalctl -xeu startup.service
# systemctl status dunst.service

# links
# https://unix.stackexchange.com/questions/85244/setting-display-in-systemd-service-file
