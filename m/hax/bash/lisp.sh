#!/bin/bash
repo="eschulte/lisp-format"
file="master/lisp-format"
dl="https://raw.githubusercontent.com/$repo/$file"
# dir=$(mktemp -d)
dir=~/m/hax/bash
out="$dir/$(basename "$file")"
wget -c "$dl" -O "$out"
chmod u+x "$out"
null=~/.local/share/nvim/site/pack/packer/start/null-ls.nvim/lua/null-ls/builtins/formatting/lisp.lua
tee "$null" << 'EOF'
local h = require("null-ls.helpers")
local methods = require("null-ls.methods")
local FORMATTING = methods.internal.FORMATTING

return h.make_builtin({
    name = "lisp",
    method = FORMATTING,
    filetypes = { "yuck" },
    generator_opts = {
        command = vim.fn.expand("~/m/hax/bash/lisp-format"),
        args = { "$FILENAME" },
        to_stdin = true,
        to_temp_file = false,
    },
    factory = h.formatter_factory,
})
EOF
