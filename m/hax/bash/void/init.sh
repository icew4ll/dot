#!/bin/bash

dir="bash/void"

init() {
  echo t
  !! | sed "s/t/a/"
  # Output: `a`
}

pkgs() {
	array=(
		deps
		utils
		xbps-src
		dwm
	)
	for i in "${array[@]}"; do
		curl "$1"/"$dir"/"$i".sh >"$i".sh
	done
}
