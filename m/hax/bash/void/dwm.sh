#!/bin/bash
source utils.sh

url=https://git.suckless.org/dwm
base=$(basename $url)
dir=/tmp/"$base"
cfg="$dir"/config.mk
cfgh="$dir"/config.h

# clean_dir "$dir"
# git clone "$url" "$dir"
# ls "$dir"

# cd /usr
# find . -path '*X11'
# find . -path '*freetype2'
cd "$dir" || exit
cp config.def.h config.h
sed -i '/^X11INC /s/=.*$/= \/usr\/include\/X11/' "$cfg"
sed -i '/^X11LIB /s/=.*$/= \/usr\/lib\/X11/' "$cfg"
sed -i '/MODKEY Mod1Mask/s/Mod1Mask/Mod4Mask/' "$cfgh"
sed -i '/borderpx  = 1/s/1/2/' "$cfgh"
sed -i 's/#005577/#282828/' "$cfgh"
sed -i '/topbar             = 1/s/1/0/' "$cfgh"
# sed -ie 's/"st"/"rofi", "-show", "drun", "-show-icons"/' config.def.h
# sed -ie 's/monospace:size=10/Agave Nerd Font Mono:size=16/' config.def.h

# build
make clean install

# configuration
# let conf = ~/.xsession
# grep 'exec' "$conf" &> /dev/null || echo "exec dwm" >> "$conf"
