#!/bin/bash

if which rg >/dev/null 2>&1; then
	entry=$(sk --ansi -i -c 'rg --color=always --line-number --no-heading --smart-case "{}"' | perl -pe 's;(.*):[0-9]+.*;\1;')
else
	exit 1
fi

case "$(file -biL "$entry")" in
	*text*)
		"${VISUAL:-$EDITOR}" "$entry"
		;;
	*)
		if uname | grep -q "Darwin"; then
			open "$entry" >/dev/null 2>&1
		else
			xdg-open "$entry" >/dev/null 2>&1
		fi
		;;
esac
