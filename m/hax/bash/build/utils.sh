#!/bin/bash
# local vars
bin="$HOME/bin"
build="$bin/build"
temp=/tmp

green_b() {
	echo -e "\e[1;42m$1\e[0m"
}

green() {
	echo -e "\e[0;32m$1\e[0m"
}

yellow() {
	echo -e "\e[1;33m$1\e[0m"
}

linky() {
	yellow "Linking...\n $1 $2"
	[ -L "$2" ] && rm "$2"
	ln -s "$1" "$2"
	green "Link Complete"
}

cloner() {
	yellow "Cloning...\n $1 $2"
	[ -d "$2" ] && rm -rf "$2"
	git clone "$1" "$2"
	cd "$2" || exit
	green "Clone Complete"
}

git_release() {
	echo "https://api.github.com/repos/$1/releases"
}

find_tag() {
	curl -sL "$1" | grep "href" | grep "$2" | perl -pe 's|.*href="(.*?)".*|\1|'
}

tar_build() {
	yellow "Downloading...\n$1"
	cd "$build" && wget -c "$1" -O - | tar -xz || exit
}

extract_gz() {
	yellow "Downloading...\n$1\nTo:\n$2"
	cd "$2" && wget -c "$1" -O - | tar -xz || exit
}

tar_tmp() {
	yellow "Downloading...\n$1"
	cd "$temp" && wget -c "$1" -O - | tar -xz || exit
}

git_downloads() {
	curl -sL "$1" | grep "browser_download_url" | perl -pe 's;.*: "(.*?)";\1;'
}

git_down() {
	curl -sL "$1"
	# curl -sL "$1" | perl -pe 's;.*: "(.*?)";\1;'
}

# get first element of tagged git api item
git_tag() {
	git_down "$1" | jq -r ".[0].$2"
}

git_tag_latest() {
	git_downloads "$1" | grep "$2" | head -1
}

clean_dir() {
	yellow "Cleaning and CD $1"
	[ -d "$1" ] && rm -rI "$1"
	mkdir -p "$1" && cd "$1" || exit
}
