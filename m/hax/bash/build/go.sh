#!/bin/bash

# https://go.dev/dl/go1.17.3.linux-amd64.tar.gz
source utils.sh

url="https://go.dev/dl"
tag="linux-amd64.tar.gz"
latest=$(find_tag "$url" "$tag" | head -1)
dl="$url/$(basename "$latest")"
bin1="bin/go"
bin2="bin/gofmt"
tar_build "$dl"
linky "$build"/go/"$bin1" "$bin"/"$(basename $bin1)"
linky "$build"/go/"$bin2" "$bin"/"$(basename $bin2)"
green "Complete"
