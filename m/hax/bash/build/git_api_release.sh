#!/bin/bash

source utils.sh

green Downloads
# https://github.com/ventoy/Ventoy/releases
repo="ventoy/Ventoy"
url=$(git_release "$repo")
dl=$(git_downloads "$url")
echo "$dl"

green Latest
tag="linux.tar.gz"
dl=$(git_tag_latest "$url" "$tag")
echo "$dl"
