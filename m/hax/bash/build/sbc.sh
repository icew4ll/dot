#!/bin/bash

source utils.sh
url="http://sbcl.org/platform-table.html"
#http://prdownloads.sourceforge.net/sbcl/sbcl-2.2.1-x86-64-linux-binary.tar.bz2
tag="x86-64-linux-binary.tar.bz2"
dl=$(find_tag "$url" "$tag" | head -1)
yellow "DL: $dl"
dir=/tmp/build
[ -d "$dir" ] && rm -rf "$dir"
mkdir -p "$dir"
cd $dir || exit
wget "$dl"
bzip2 -cd sbcl-* | tar xvf -
cd sbcl-*-x86-64-linux || exit
sudo sh install.sh
green "COMPLETE"
