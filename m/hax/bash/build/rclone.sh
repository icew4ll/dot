#!/bin/bash

source utils.sh

url="https://github.com/rclone/rclone"
base=$(basename $url)
dir="$build"/"$base"
cloner "$url" "$dir"
go build
linky "$dir"/"$base" "$bin"/"$base"
green "Complete"
