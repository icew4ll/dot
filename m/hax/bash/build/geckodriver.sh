#!/bin/bash

source utils.sh

repo="mozilla/geckodriver"
url=$(git_release $repo)
tag="linux64.tar.gz"
base=$(basename "$repo")
# dl=$(git_downloads "$url")
dl=$(git_tag_latest "$url" "$tag")
echo "$dl"
tar_build "$dl"
linky "$build"/"$base" "$bin"/"$base"
green "Complete"
