#!/bin/bash
slap deps ffmpeg || exit 1

eval "$(
	slap parse bash _ -- "$@" <<-EOF
		name: song
		version: "1.0"
		about: downloads song
		
		settings:
		    - ArgRequiredElseHelp
		    - ColorAuto
		
		global_settings:
		    - ColoredHelp
		
		args:
		    - file:
		        help: file
		        required: true
		        takes_value: true
		    - begin:
		        help: begin 
		        long: begin
		        short: b
		        required: true
		        takes_value: true
		    - duration:
		        help: duration
		        long: duration
		        short: d
		        required: true
		        takes_value: true
		    - out:
		        help: out
		        long: out
		        short: o
		        required: true
		        takes_value: true
	EOF
)"

[[ -z ${_success} ]] && exit 1

echo \
	"${_file_vals}" \
	"${_begin_vals}" \
	"${_duration_vals}" \
	"${_duration_out}"

ffmpeg -i "${_file_vals}" \
	-ss "${_begin_vals}" \
	-to "${_duration_vals}" \
	-c copy "${_out_vals}".m4a
