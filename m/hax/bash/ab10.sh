#!/bin/expect

set prompt {[#|%|>|$] $}
spawn ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no $env(UI)@$env(AMBK10)
expect $prompt
interact
