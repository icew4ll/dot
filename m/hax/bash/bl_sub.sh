#!/bin/bash

expect <<EOF
set prompt {[#|%|>|\$] \$}
spawn ssh $UI@216.230.241.161
expect "assword"
send "$PG\n"
expect \$prompt
send "telnet 10.254.0.54\n"
expect "assword"
send "1FdQAzBC\n"
expect \$prompt
send "en\n\n"
expect \$prompt
send "conf t\n"
expect \$prompt
send "object-group network blocklist\n"
expect \$prompt
send "network-object $1\n"
expect \$prompt
send "write\n"
expect \$prompt
send "exit\n"
expect \$prompt
send "exit\n"
expect \$prompt
send "exit\n"
expect \$prompt
send "exit\n"
expect eof
EOF
