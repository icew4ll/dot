#!/bin/bash

name=arch
mb=2048
# iso=~/vm/iso/archlinux-2022.02.01-x86_64.iso
iso=/var/lib/libvirt//archlinux-2022.02.01-x86_64.iso
diskgb=15
disk=~/vm/img/arch.qcow2
cpus=2

sudo cp ~/vm/iso/archlinux-2022.02.01-x86_64.iso /var/lib/libvirt/images/
# sudo ls -lah /var/lib/libvirt/images/

virt-install \
	--name "$name" \
	--ram "$mb" \
	--disk path="$disk,size=$diskgb" \
	--vcpus "$cpus" \
	--os-type linux \
	--os-variant generic \
	--graphics none \
  --bridge=br0 \
	--console pty,target_type=serial \
	--location "$iso" \
	--extra-args 'console=ttyS0,115200n8 serial'
