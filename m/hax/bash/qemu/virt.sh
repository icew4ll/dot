#!/bin/bash
virt-install \
  --name test \
  --ram 4096 \
  --disk path="/home/ice/vm/img/IE11 - Win81.qcow2" \
  --os-type=windows \
  --os-variant=win8.1 \
  --vcpus 2 \
