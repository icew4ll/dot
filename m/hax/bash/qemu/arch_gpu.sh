#!/bin/bash
# config uses default bridge br0
img=~/vm/img/linux.qcow2
iso=~/vm/iso/archlinux-2022.02.01-x86_64.iso
bios="/gnu/store/h85g6g7cllis5g4fdj2swqcnpgnzx6sd-ovmf-20170116-1.13a50a6/share/firmware/ovmf_x64.bin"
size=25G
ram=8192
# ram=4096
# -smp cores=4,threads=1 \
# working:
# -net nic -net bridge,br="$br" \
# -device virtio-net-pci,netdev=hn0,id=nic1 \
# br=br0

# create qcow2
[ ! -f "$img" ] && qemu-img create -f qcow2 "$img" "$size"

# create vm
qemu-system-x86_64 \
	-bios "$bios" \
	-cpu host \
	-enable-kvm \
	-smp cores="$(nproc)",threads=1,sockets=1 \
	-rtc clock=host,base=localtime \
	-device virtio-rng-pci \
	-mem-path /dev/hugepages \
	-mem-prealloc \
	-k en-us \
	-machine kernel_irqchip=on \
	-global PIIX4_PM.disable_s3=1 -global PIIX4_PM.disable_s4=1 \
	-parallel none -serial none \
	-vga none \
	-display gtk,gl=on \
	-soundhw all \
	-nic user,model=virtio-net-pci,hostfwd=tcp::10022-:22 \
	-boot menu=on,order=d \
	-drive file="$img" \
	-drive media=cdrom,file="$iso" \
	-m "$ram"

# https://adamgradzki.com/2020/04/06/faster-virtual-machines-linux/
