#!/bin/bash
# worked after reboot, network issue?
# after running adds tap0
# bridge network allows vm access to local network
# virbr0 virt-manager virtual bridge (virtual switch)
# vnet0 virtual nic
# -net nic \
# -net bridge,br=$br \
# -smp cores=4,threads=1
# img="$HOME/vm/img/IE11 - Win81.qcow2"
# -netdev bridge,br=br0,id=net0 \
img="/home/ice/vm/img/IE11 - Win81.qcow2"
ram=8192
br=br0
bios="/gnu/store/h85g6g7cllis5g4fdj2swqcnpgnzx6sd-ovmf-20170116-1.13a50a6/share/firmware/ovmf_x64.bin"
qemu-system-x86_64 \
	-bios "$bios" \
	-enable-kvm \
	-cpu host \
	-smp 1 \
	-machine type=pc,accel=kvm \
	-vga virtio \
	-net nic -net bridge,br="$br" \
	-boot menu=on,order=d \
	-drive file="$img" \
	-m "$ram"
