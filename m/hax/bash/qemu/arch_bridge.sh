#!/bin/bash
# config uses default bridge br0
img=~/vm/img/linux.qcow2
iso=~/vm/iso/archlinux-2022.02.01-x86_64.iso
bios="/gnu/store/h85g6g7cllis5g4fdj2swqcnpgnzx6sd-ovmf-20170116-1.13a50a6/share/firmware/ovmf_x64.bin"
size=25G
ram=8192
# ram=4096
# -smp cores=4,threads=1 \
# working:
# -net nic -net bridge,br="$br" \
# -device virtio-net-pci,netdev=hn0,id=nic1 \
br=br0

# create qcow2
[ ! -f "$img" ] && qemu-img create -f qcow2 "$img" "$size"

# create vm
qemu-system-x86_64 \
	-bios "$bios" \
	-enable-kvm \
	-cpu host \
	-smp cores=4,threads=1 \
	-vga virtio \
  -nic bridge,br=$br,model=virtio-net-pci \
	-boot menu=on,order=d \
	-drive file="$img" \
	-drive media=cdrom,file="$iso" \
	-m "$ram"
