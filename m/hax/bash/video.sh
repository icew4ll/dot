#!/bin/bash

dir=~/Videos/cam
ext=dav
out=mp4

filer() {
	cd $dir || exit
	fd -0 . \
		-t f \
		-e $ext
}
filer

filer | while IFS= read -r -d '' el; do
	file=${el%.*}
	ffmpeg \
		-y \
		-nostdin \
		-i $file.$ext \
		-c:v libx264 \
		-crf 24 \
		$file.$out
done

# LNR6108X_ch2_main_20210818193000_20210818194500.mp4
# 7:42:05
# 7:42:15
# /media/ice/usb/LNR6108X_ch3_main_20210819010000_20210819043000.mp4
# 2:11:00

# $ s=/the/path/foo.txt
# $ echo "${s##*/}"
# foo.txt
# $ s=${s##*/}
# $ echo "${s%.txt}"
# foo
# $ echo "${s%.*}"
# foo
