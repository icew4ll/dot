#!/bin/bash

# fix steam
file=/etc/pacman.conf
pat="\[multilib\]"
sudo sed -i "/$pat/s/^#//" "$file"
sudo sed -i "/$pat/a Include = /etc/pacman.d/mirrorlist" "$file"
grep "$pat" "$file" -A 5
sudo pacman -Sy
sudo pacman --needed --noconfirm -S steam noto-fonts amdvlk lib32-amdvlk
