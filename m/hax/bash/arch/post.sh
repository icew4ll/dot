#!/bin/bash

pacman -Syu
uname -a
pacman -Ss xfce
pacman --noconfirm -S xfce4 xfce4-goodies
pacman --noconfirm -S sddm ttf-dejavu
systemctl enable --now sddm.service -f
systemctl restart sddm.service

file=/etc/sddm.conf.d/autologin.conf
mkdir -p "$(dirname $file)"
tee $file << EOF
[Autologin]
User=arch
EOF

pacman -S virt-manager
