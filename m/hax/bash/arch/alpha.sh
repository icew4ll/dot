#!/bin/bash

green() {
	echo -e "\e[0;32m$1\e[0m"
}

yellow() {
	echo -e "\e[1;33m$1\e[0m"
}

yellow init
file=/etc/pacman.conf
opt=Color
sed -i "/$opt/s/^#//g" "$file"
systemctl start reflector
timedatectl set-ntp true
pacman -Sy --needed --noconfirm cryptsetup btrfs-progs gdisk tree which neovim glibc
INST_TZ=/usr/share/zoneinfo/America/Los_Angeles
INST_HOST='earth'
DISK=/dev/disk/by-id/ata-QEMU_HARDDISK_QM00001
INST_MNT=$(mktemp -d)

yellow partition
sgdisk --zap-all "$DISK"
sgdisk -n1:1M:+1G -t1:EF00 "$DISK"
sgdisk -a1 -n5:24K:+1000K -t5:EF02 "$DISK"
sgdisk -n2:0:0 "$DISK"

yellow crypt
boot_partuuid=$(blkid -s PARTUUID -o value "$DISK-part2")
boot_mapper_name="cryptroot-luks1-partuuid-$boot_partuuid"
boot_mapper_path="/dev/mapper/$boot_mapper_name"

while [ ! -e "$DISK-part2" ]; do
	sleep 2
	# Format and open LUKS container
	cryptsetup luksFormat --type luks1 "$DISK-part2"
	cryptsetup open "$DISK"-part2 "$boot_mapper_name"
	# format LUKS container as btrfs
	mkfs.btrfs "$boot_mapper_path"
	mount "$boot_mapper_path" "$INST_MNT"
	lsblk -f
done

yellow subvols
cd "$INST_MNT" || exit
btrfs subvolume create @
mkdir @/0
btrfs subvolume create @/0/snapshot
for i in {home,root,srv,usr,usr/local,swap,var}; do
	btrfs subvolume create @"$i"
done
# exclude these dirs under /var from system snapshot
for i in {tmp,spool,log}; do
	btrfs subvolume create @var/"$i"
done
btrfs subvolume list "$INST_MNT"

yellow "mount subvols"
cd ~ || exit
umount "$INST_MNT"
mount "$boot_mapper_path" "$INST_MNT" -o subvol=/@/0/snapshot,compress-force=zstd,noatime,space_cache=v2
# create dirs
mkdir -p "$INST_MNT"/{.snapshots,home,root,srv,tmp,usr/local,swap}
mkdir -p "$INST_MNT"/var/{tmp,spool,log}
# mount snaps
mount "$boot_mapper_path" "$INST_MNT"/.snapshots/ -o subvol=@,compress-force=zstd,noatime,space_cache=v2
lsblk -f
# mount subvolumes
# separate /{home,root,srv,swap,usr/local} from root filesystem
for i in {home,root,srv,swap,usr/local}; do
	mount "$boot_mapper_path" "$INST_MNT/$i" -o subvol=@"$i",compress-force=zstd,noatime,space_cache=v2
done
# separate /var/{tmp,spool,log} from root filesystem
for i in {tmp,spool,log}; do
	mount "$boot_mapper_path" "$INST_MNT/var/$i" -o subvol=@var/"$i",compress-force=zstd,noatime,space_cache=v2
done

yellow cow
for i in {swap,}; do
	chattr +C "$INST_MNT/$i"
done

yellow efi
mkfs.vfat -n EFI "$DISK"-part1
mkdir -p "$INST_MNT"/boot/efi
mount "$DISK"-part1 "$INST_MNT"/boot/efi

yellow strap
# basestrap (artix), pacstrap (arch)
pacstrap "$INST_MNT" base neovim mandoc grub cryptsetup btrfs-progs snapper snap-pac grub grub-btrfs openssh
chmod 750 "$INST_MNT"/root
chmod 1777 "$INST_MNT"/var/tmp/
# system
pacstrap "$INST_MNT" linux-zen linux-zen-headers
pacstrap "$INST_MNT" linux-firmware dosfstools efibootmgr intel-ucode

yellow fstab
# genfstab (arch) fstabgen(artix)
genfstab -U "$INST_MNT" >"$INST_MNT"/etc/fstab
# remove hard-coded sys subvol
sed -i "s|,subvolid=257,subvol=/@/0/snapshot||g" "$INST_MNT"/etc/fstab

yellow key
mkdir -p "$INST_MNT"/lukskey
dd bs=512 count=8 if=/dev/urandom of="$INST_MNT"/lukskey/crypto_keyfile.bin
chmod 600 "$INST_MNT"/lukskey/crypto_keyfile.bin
cryptsetup luksAddKey "$DISK-part2" "$INST_MNT"/lukskey/crypto_keyfile.bin
chmod 700 "$INST_MNT"/lukskey

yellow initramfs
cryptkey=/lukskey/crypto_keyfile.bin
mv "$INST_MNT"/etc/mkinitcpio.conf "$INST_MNT"/etc/mkinitcpio.conf.original
tee "$INST_MNT"/etc/mkinitcpio.conf <<EOF
BINARIES=(/usr/bin/btrfs)
FILES=($cryptkey)
HOOKS=(base udev autodetect modconf block encrypt filesystems keyboard fsck grub-btrfs-overlayfs)
EOF

yellow grub
echo "GRUB_ENABLE_CRYPTODISK=y" >>"$INST_MNT"/etc/default/grub
echo "GRUB_CMDLINE_LINUX=\"cryptdevice=PARTUUID=$boot_partuuid:$boot_mapper_name root=$boot_mapper_path cryptkey=rootfs:$cryptkey\"" >>"$INST_MNT"/etc/default/grub

yellow host
echo "$INST_HOST" >"$INST_MNT"/etc/hostname
INET=ens3
tee "$INST_MNT"/etc/systemd/network/20-default.network <<EOF

[Match]
Name=$INET

[Network]
DHCP=yes
EOF
ln -sf "$INST_TZ" "$INST_MNT"/etc/localtime
hwclock --systohc
echo "en_US.UTF-8 UTF-8" >>"$INST_MNT"/etc/locale.gen
echo "LANG=en_US.UTF-8" >>"$INST_MNT"/etc/locale.conf

yellow chroot
user=flat
arch-chroot "$INST_MNT" bash <<EOF
# apply locals
locale-gen

# enable connman
systemctl enable systemd-networkd systemd-resolved

# root pass
echo -e "arch\narch" | passwd

# initramfs
mkinitcpio -P

# btrfs service
systemctl enable grub-btrfs.path

# user
useradd -s /bin/bash -U -G wheel,video -m --btrfs-subvolume-home "$user"

# snapper
lsblk -f
umount /.snapshots/
rmdir /.snapshots/
snapper --no-dbus -c root create-config /
rmdir /.snapshots/
mkdir /.snapshots/
mount /.snapshots/
snapper --no-dbus -c home create-config /home/
snapper --no-dbus -c "$user" create-config /home/"$user"
systemctl enable /lib/systemd/system/snapper-*

# grub init
grub-install
grub-install --removable
grub-install "$DISK"
grub-mkconfig -o /boot/grub/grub.cfg

# confirm fstab
cat /etc/fstab
EOF

yellow final
mount | grep "$INST_MNT/" | tac | cut -d" " -f3 | xargs -i{} umount -lf {}
umount "$INST_MNT"
cryptsetup close "$boot_mapper_name"
