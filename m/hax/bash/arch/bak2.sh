#!/bin/bash

green() {
	echo -e "\e[0;32m$1\e[0m"
}

yellow() {
	echo -e "\e[1;33m$1\e[0m"
}

INST_TZ=/usr/share/zoneinfo/America/Los_Angeles
INST_HOST='earth'
DISK=/dev/disk/by-id/ata-QEMU_HARDDISK_QM00001
INST_MNT=$(mktemp -d)
BOOT_PARTUUID=$(blkid -s PARTUUID -o value "$DISK-part2")
BOOT_MAPPER_NAME="cryptroot-luks1-partuuid-$BOOT_PARTUUID"
BOOT_MAPPER_PATH="/dev/mapper/$BOOT_MAPPER_NAME"
BTRFS_OPTS="rw,noatime,compress=zstd,space_cache=v2,discard=async,commit=120"
yellow "$DISK"
yellow "$INST_MNT"
yellow "$BOOT_PARTUUID"
yellow "$BOOT_MAPPER_NAME"
yellow "$BOOT_MAPPER_PATH"

yellow INIT
# uncomment Color option
file=/etc/pacman.conf
opt=Color
sed "/$opt/s/^#//g" "$file"

# preqs
pacman -Sy --needed --noconfirm cryptsetup btrfs-progs gdisk tree which glibc

yellow PARTITION
# sda      8:0    0    50G  0 disk
# ├─sda1   8:1    0     1G  0 part
# ├─sda2   8:2    0    49G  0 part
# └─sda5   8:5    0  1000K  0 part
# clear partition table
sgdisk --zap-all "$DISK"
# create efi
sgdisk -n1:1M:+1G -t1:EF00 "$DISK"
# bios boot partition
sgdisk -a1 -n5:24K:+1000K -t5:EF02 "$DISK"
# main partition
sgdisk -n2:0:0 "$DISK"
lsblk -f

yellow FORMAT
# Format and open LUKS container
while [ ! -e "$DISK-part2" ]; do
  yellow SLEEP
	sleep 2
	counter=$((counter + 1))
  cryptsetup luksFormat --type luks1 "$DISK-part2"
  cryptsetup open "$DISK"-part2 "$BOOT_MAPPER_NAME"
done
cryptsetup close "$BOOT_MAPPER_NAME"
