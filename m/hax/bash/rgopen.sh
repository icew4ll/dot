#!/bin/bash

if which rg >/dev/null 2>&1; then
	RG_PREFIX="rg --column --line-number --no-heading --color=always --smart-case "
	# RG_PREFIX="rg"
	INITIAL_QUERY=""
	entry=$(sk --bind "change:reload:$RG_PREFIX {q} || true" --ansi --query "$INITIAL_QUERY" | perl -pe 's;(.*):[0-9]+:[0-9]+:.*;\1;')
else
	exit 1
fi

case "$(file -biL "$entry")" in
	*text*)
		"${VISUAL:-$EDITOR}" "$entry"
		;;
	*)
		if uname | grep -q "Darwin"; then
			open "$entry" >/dev/null 2>&1
		else
			xdg-open "$entry" >/dev/null 2>&1
		fi
		;;
esac
