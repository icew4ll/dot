#!/bin/bash

dev=/dev/sda

status() {
	lsblk -f
}

green() {
	echo -e "\e[0;32m$1\e[0m"
}

partition() {
	# write partitions
	wipefs -a $dev
	printf "g\nn\n1\n\n+260M\nt\n1\nn\n\n\n\nw\n" | fdisk "$dev"
}

format() {
	# efi
	mkfs.fat $dev"1"

	# format
	mkfs.ext4 $dev"2"

}

mounts() {
	mount $dev"2" /mnt
	mkdir -p /mnt/boot/efi
	mount -o rw,noatime $dev"1" /mnt/boot/efi
}

swap() {
	swap=/mnt/var/swap/swapfile
	# Create a zero length file that will serve as the swapfile
	truncate -s 0 $swap

	# Set permissions for the swap file
	chmod 600 $swap

	# Create the swap file
	count=$(free -m | awk 'FNR == 2 {print $2}' | awk '{print $1 + int(sqrt($1))}')
	echo "$count"
	dd if=/dev/zero of=$swap bs=1M count="$count" status=progress conv=fdatasync

	# Format the swap file
	mkswap "$swap"
}

channels() {
	file=~/.config/guix/base-channels.scm
	mkdir -p "$(dirname $file)"
	tee $file <<EOF
(cons* (channel
        (name 'nvidia)
        (url "https://gitlab.com/squarerectangle/nvidiachannel"))
       (channel
        (name 'nonguix)
        (url "https://gitlab.com/nonguix/nonguix")
        ;; Enable signature verification:
        (introduction
         (make-channel-introduction
          "897c1a470da759236cc11798f4e0a5f7d4d59fbc"
          (openpgp-fingerprint
           "2A39 3FFF 68F4 EF7A 3D29  12AF 6F51 20A0 22FB B2D5"))))
       %default-channels)
EOF
	guix pull --channels=$file
	file=~/.config/guix/channels.scm
	guix describe --format=channels >$file
	hash guix
	green "$(guix describe)"
}

config() {
	efi=$(lsblk -o PATH,UUID | grep -E "(sda1)" | awk '{print $2}')
	root=$(lsblk -o PATH,UUID | grep -E "(sda2)" | awk '{print $2}')
	ssh="ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCqfZkR3Pk6Wvp/T//XhnUWHNh75ZdQzrlAHrvd/bjC3lAHVv7uqj1g8ajpco/0WwSDZXcb2CAyIwBwepAW2tXuFL2I6xDQJHDAcBF28IZM0QXGJ2bqtVHfz7MiwisYczDj9RzuWNVkg/aOeLTM3k5tu0IIcjhRnuZu8xozuEzRTL7gnKM3+mS1Fe91pkD6VA8HFOjQ2Hk/v5baVqQAL+NzRBHz+6/JKeDY2d/xWVJHluQL/MOYCZIoDGJaBqjI5jLHhFykFJeXixXBJ4+kzHuqAznug6W3zg2Fo2/Om2oAcnuhzn0dic2p60BaB0YR0UgULXnVePZu4b2cb4J5kLkABvlHvSPJQZrXa7zUAJYgdmlnH/R/W9EDI8aA6JwuppjKtbiDxjOdUs7CXy2JjnkxaXDcYb7484RwX+mkY+vgOTy6i3pGwXQ9fj750g5PbmJQs9rbIBlFg82fr8Z+iCdO2ICcFvErwVYgl97uPTmKtfZ2AS4wT9y4+qKBt62i4wSZUJTxIS1/6NaH1HtePHWj+5/3w6PU53swY1/1t88O3Zt7R0gKW1nzTKY8WBRj08h/9MGMeNRkFfss28ytd5H1jysrKMI/2zvyxzFcJiTBCuCKPVIVeOktzArUnC7OtOa6lhIBOU7DGpqt6E3/7IZDa03AVIFBJPOKtIs1ExeicQ== ice@wall"
	file=test.scm
	tee $file <<EOF
(use-modules
 (gnu)
 (gnu system nss)
 (nongnu packages linux)
 (nongnu system linux-initrd)
 (srfi srfi-1)
 )
(use-service-modules xorg
                     linux
                     desktop ;; %desktop-services
                     networking ;; modem-manager-service-type
                     ssh ;; ssh
                     )
(use-package-modules xorg wm certs
                     display-managers
                     freedesktop ;; libinput
                     suckless ;; st
                     )

;; make sure you downloaded this file too
(load "./gtransform.scm")

(define %root-ssh-public-key
  "$ssh")

;; special xorg config.
;; adds nvidia xorg module and transforms xorg-server package
(define my-xorg-conf
  (xorg-configuration
   (modules
    (cons*
     (fixpkg nvidia-libs-minimal)
     ;; optional: remove garbage.
     (remove
      (lambda (pkg)
        (member pkg
                (list
                 xf86-video-amdgpu
                 xf86-video-ati
                 xf86-video-cirrus
                 xf86-video-intel
                 xf86-video-mach64
                 xf86-video-nouveau
                 xf86-video-nv
                 xf86-video-sis)))
      %default-xorg-modules)))
   (server (fixpkg xorg-server))
   (drivers '("nvidia"))))

(operating-system
 (locale "en_US.utf8")
 (timezone "America/Los_Angeles")
 (keyboard-layout (keyboard-layout "us"))
 (host-name "earth")

 (initrd microcode-initrd)
 (initrd-modules %base-initrd-modules)

 (kernel (fixpkg linux))
 (kernel-loadable-modules (list (fixpkg nvidia-module)))
 (kernel-arguments (list
                    ;; enable a feature
                    "nvidia-drm.modeset=1"
                    ;; nvidia_uvm gives me problems
                    "modprobe.blacklist=nouveau,nvidia_uvm"))
 (firmware (fixpkgs (list linux-firmware)))

 (bootloader
  (bootloader-configuration
   (bootloader grub-efi-bootloader)
   (targets (list "/boot/efi"))
   (keyboard-layout keyboard-layout)))
 (file-systems
  (cons* (file-system
          (mount-point "/boot/efi")
          (device (uuid "$efi" 'fat16))
          (type "vfat"))
         (file-system
          (mount-point "/")
          (device
           (uuid "$root"
                 'ext4))
          (type "ext4"))
         %base-file-systems))

 (users (cons* (user-account
                (name "flat")
                (comment "Flat")
                (group "users")
                (home-directory "/home/flat")
                (supplementary-groups
                 '("wheel" "netdev" "audio" "video")))
               %base-user-accounts))

 (packages
  (fixpkgs
   (cons*
    nss-certs

    ;; does this actually have to be global? i don't know. i do it anyway.
    nvidia-libs-minimal

    ;; are these even needed? i don't remember.
    xf86-input-libinput
    libinput

    ;; packages so you can actually test to see if it works
    awesome ;; window manager
    st

    %base-packages)))
 (name-service-switch %mdns-host-lookup-nss)
 (services
  (cons*
   ;; nvidia udev service
   (simple-service
    'my-nvidia-udev-rules udev-service-type
    (list (fixpkg nvidia-udev)))

   (service openssh-service-type
            (openssh-configuration
             (permit-root-login 'prohibit-password)
             (password-authentication? #f)
             (authorized-keys
              \`(("root" ,(plain-file "authorized_keys"
                                     %root-ssh-public-key))))))

   ;; add liglvnd slim using special xorg config
   (service slim-service-type
            (slim-configuration
             (slim (fixpkg slim))
             (xorg-configuration my-xorg-conf)))

   (service kernel-module-loader-service-type
            '("nvidia"
              "nvidia_modeset"
              ;; i dont remember why i put this one here.
              ;; i think i stole it from somebody else.
              ;; maybe it's not needed.
              "ipmi_devintf"))

   (service dhcp-client-service-type)
   
   ;; default services, with some packages removed for faster build
   (remove (lambda (service)
             (or (member (service-kind service)
                         (list
                          gdm-service-type
                          modem-manager-service-type
                          usb-modeswitch-service-type
                          wpa-supplicant-service-type
                          network-manager-service-type
                          geoclue-service-type
                          colord-service-type))
                 (member
                  (struct-ref (service-kind service) 0)
                  '(
                    screen-locker
                    mount-setuid-helpers
                    network-manager-applet
                    ))))
           %desktop-services))))
EOF
	herd start cow-store /mnt
	guix system init $file /mnt
	# guix system init $file /mnt --no-substitutes
  # substitute
	# url="https://guix.tobias.gr/signing-key.pub"
	# curl -sL $url | sudo guix archive --authorize
	# guix system init $file /mnt --substitute-urls="https://guix.tobias.gr"
	# ECHO REBOOTING NOW
	# reboot
}

main() {
	# sanity
	partition
	format
	mounts
	swap
	status
	channels
	config
}
main
