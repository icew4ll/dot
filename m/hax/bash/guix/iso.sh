#!/bin/bash

green() {
	echo -e "\e[0;32m$1\e[0m"
}

setup() {
	# vars
	dir=/tmp/iso
	base=$dir/base.scm
	chan=$dir/chan.scm
	man=$dir/man.scm
	isos=~/vm/iso

	# working dir
	[ ! -d "$dir" ] && mkdir "$dir"
	cd "$dir" || exit 1
}

channels() {
	# base channels
	tee "$base" <<EOF
(cons* (channel
        (name 'nvidia)
        (url "https://gitlab.com/squarerectangle/nvidiachannel"))
       (channel
        (name 'nonguix)
        (url "https://gitlab.com/nonguix/nonguix")
        ;; Enable signature verification:
        (introduction
         (make-channel-introduction
          "897c1a470da759236cc11798f4e0a5f7d4d59fbc"
          (openpgp-fingerprint
           "2A39 3FFF 68F4 EF7A 3D29  12AF 6F51 20A0 22FB B2D5"))))
       %default-channels)
EOF

	# update base channels to latest commits
	guix pull --channels="$base"
	guix describe --format=channels >"$chan"

}

man() {
	tee "$man" <<EOF
(define-module (nongnu system install)
  #:use-module (gnu packages bootloaders)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages mtools)
  #:use-module (gnu packages package-management)
  #:use-module (gnu packages version-control)
  #:use-module (gnu packages vim)
  #:use-module (gnu packages) ;; for specification->package
  #:use-module (gnu services base)
  #:use-module (gnu services)
  #:use-module (gnu system file-systems)
  #:use-module (gnu system install) ;; for inherit installation-os
  #:use-module (gnu system)
  #:use-module (guix gexp) ;; for file-append
  #:use-module (guix)
  #:use-module (nongnu packages linux)
  #:export (installation-os-nonfree))

(define installation-os-nonfree
  (operating-system
   (inherit installation-os)
   (kernel linux)
   (firmware (list linux-firmware))

   ;; Add the 'net.ifnames' argument to prevent network interfaces
   ;; from having really long names.  This can cause an issue with
   ;; wpa_supplicant when you try to connect to a wifi network.
   (kernel-arguments '("quiet" "modprobe.blacklist=radeon" "net.ifnames=0" "console=ttyS0"))

   (services
    (cons*
     (simple-service 'channel-file etc-service-type
                     (list \`("chan.scm" ,(local-file "chan.scm"))))
     (operating-system-user-services installation-os)))

   ;; Add some extra packages useful for the installation process
   (packages (append
              (map specification->package+output
                   ;; System packages
                   '("vim"
                     "curl" "wget" "git" "fd" "ripgrep"
                     "tmux" "perl" "nnn" "rsync"
                     "grub" "glibc" "nss-certs"))
              %base-packages-disk-utilities
              %base-packages))
   )
  )
installation-os-nonfree
EOF

	# build image per manifest
	guix time-machine -C "$chan" -- system image -t iso9660 "$man"
}

copy() {
	# cp image
	tmp="$(fd . /gnu/store -e iso)"
	iso="$isos"/nonguix-"$(date '+%Y-%m-%d')".iso
	# green "$tmp" "$iso"
	cp "$tmp" "$iso"

	# clean up iso
	guix package --delete-generations
	guix gc --collect-garbage
}

main() {
  setup
  channels
  man
  copy
}
main
