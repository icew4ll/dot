#!/bin/bash
file=~/.local/share/nvim/site/pack/packer/start/null-ls.nvim/lua/null-ls/builtins/formatting/lisp.lua

tee "$file" << EOF
local h = require("null-ls.helpers")
local methods = require("null-ls.methods")
local FORMATTING = methods.internal.FORMATTING

return h.make_builtin({
    name = "lisp",
    method = FORMATTING,
    filetypes = { "yuck" },
    generator_opts = {
    command = vim.fn.expand("~/m/hax/bash/indent-code.el"),
        args = { "$FILENAME" },
        to_stdin = false,
        to_temp_file = true,
    },
    factory = h.formatter_factory,
})
EOF
