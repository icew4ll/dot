elvish ~/m/hax/elvish/hash.elv
var dir = (fd . $E:BOOKMARKS_DIR -x elvish -c 'printf "%10s -> %s\n" (basename {}) (readlink -f {})' | sk | awk '{print $3}')
if ?(test -d $dir) {
  cd $dir
}
