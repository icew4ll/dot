# map bookmarks
var book = [
&vid=~/Videos
&vm=~/m/vim
&pj=~/m
&git=~/m/vim/git
&guix=~/m/guix
&Pictures=~/Pictures
&Pictures=~/Pictures/flat/covid
&Pictures=~/Pictures/unix
]

# clean bookmarks
cd $E:BOOKMARKS_DIR
fd -x rm

# gen links
keys $book | each {
|key|
ln -s $book[$key] $key
}
