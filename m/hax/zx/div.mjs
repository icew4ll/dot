#!/usr/bin/env zx

// let s = `Divinity_track7.m4a`
// let s = `Divinity - Original Sin - OST - Soundtrack (Full Tracklist)-18i_g9knULM.m4a`
let s = String.raw`Divinity\ -\ Original\ Sin\ -\ OST\ -\ Soundtrack\ \(Full\ Tracklist\)-18i_g9knULM.m4a`
let file = `${os.homedir()}/Music/game/${s}`
let cmd = `ffmpeg -i ${file} -ss ${await question('Begin?')} -to ${await question('End?')} -c copy ${await question('Out?')}.mp4`
console.log(chalk.blue(cmd))
$.shell = '/usr/bin/zsh'

try {
  await $`'${cmd}'`
} catch (p) {
  console.log(`Exit code: ${p.exitCode}`)
  console.log(`Error: ${p.stderr}`)
}
