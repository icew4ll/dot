#!/usr/bin/env zx

let eof = `expect --help`
let evar = process.env.PG
console.log(chalk.blue(`echo: ${evar}`))

let ls = await $`
expect -c '
set prompt {[#|%|>|\$] \$}
spawn ssh pspinc@216.230.241.161
expect "assword"
send "${evar}\n"
expect \$prompt
send "exit\n"
expect eof
'`

// expect <<EOF
// set prompt {[#|%|>|\$] \$}
// spawn ssh $UI@216.230.241.161
// expect "assword"
// send "$PG\n"
// expect \$prompt
// send "exit\n"
// expect eof
// EOF
