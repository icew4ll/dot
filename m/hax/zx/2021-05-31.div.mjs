#!/usr/bin/env zx

// let dir = `${os.homedir()}/Music/game/Di`
// let s = String.raw`Divinity\ -\ Original\ Sin\ -\ OST\ -\ Soundtrack\ \(Full\ Tracklist\)-18i_g9knULM.m4a`
let s = String.raw`Divinity - Original Sin - OST - Soundtrack (Full Tracklist)-18i_g9knULM.m4a`
let file = `'${os.homedir()}/Music/game/${s}'`
// let res = await $`ls -lah ${dir}`
// let res = await $`ffmpeg -i ${file}`
// let args = process.argv.slice(2)
// let res = await $`echo ${args}`
// console.log(chalk.blue(`Files count: ${res}`))

// let questions = ["Begin", "End", "Out"]
// 
// class Song {
//   constructor(begin, end) {
//     this.begin = begin;
//     this.end = end;
//   }
// }
// 
// let b = await question('Begin?')
// let e = await question('End?')
// let o = await question('End?')
// let s = new Song(b, e)
// console.log(s)
// // beginning
// let begin = await question('Begin?')
// console.log(chalk.blue(`Begin: ${await question('Begin?')}`))
// 
// // end
// let end = await question('End?')
// console.log(chalk.blue(`End: ${end}`))
// 
// // end
// let out = await question('End?')
// console.log(chalk.blue(`Out: ${out}`))
let cmd = `/usr/bin/ffmpeg -i ${file} -ss ${await question('Begin?')} -to ${await question('End?')} -c copy ${await question('Out?')}.mp4`
console.log(chalk.blue(cmd))
await $`${cmd}`
